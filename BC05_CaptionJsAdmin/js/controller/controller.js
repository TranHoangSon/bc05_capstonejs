function renderShopList(Shop) {
  var contentHTML = "";
  Shop.forEach(function (item) {
    var contentTr = `
      <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.price}</td>
        <td>${item.screen}</td>
        <td>${item.backCamera}</td>
        <td>${item.frontCamera}</td>
        <td><img class="width="140" height="140"" src="${item.img}" alt=""></td>
        <td>${item.desc}</td>
        <td>${item.type}</td>
        <td><button class= "btn btn-danger" onclick="removeShop('${item.id}')" class="btn btn-danger">Delete</button></td>
        <td><button class= "btn btn-success" onclick="editShopList('${item.id}')" class="btn btn-danger">Edit</button></td>
        <td></td>
        
      </tr>`;

    contentHTML += contentTr;
  });
  document.getElementById("tbody_shop").innerHTML = contentHTML;
}

function timViTri(id, xoaNV) {
  for (var index = 0; index < xoaNV.length; index++) {
    var item = xoaNV[index];

    if (item.id == id) {
      return index;
    }
  }
  return -1;
}

function layThongTinTuForm() {
  var id = document.getElementById("id").value;
  var name = document.getElementById("name").value;
  var price = document.getElementById("price").value;
  var screen = document.getElementById("screen").value;
  var backCamera = document.getElementById("backCamera").value;
  var frontCamera = document.getElementById("frontCamera").value;
  var img = document.getElementById("img").value;
  var desc = document.getElementById("desc").value;
  var type = document.getElementById("type").value;

  var data = new shop(
    id,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type
  );
  return data;
}

function showThongTinLenForm(edit) {
  document.getElementById("id").value = edit.id;
  document.getElementById("name").value = edit.name;
  document.getElementById("price").value = edit.price;
  document.getElementById("screen").value = edit.screen;
  document.getElementById("backCamera").value = edit.backCamera;
  document.getElementById("frontCamera").value = edit.frontCamera;
  document.getElementById("img").value = edit.img;
  document.getElementById("desc").value = edit.desc;
  document.getElementById("type").value = edit.type;
}

function showMessageErr(idErr, message) {
  document.getElementById(idErr).innerHTML = message;
}
