function kiemTraRong(userInput, idErr, message) {
  if (userInput.length == 0) {
    showMessageErr(idErr, message);
    return false;
  } else {
    showMessageErr(idErr, "");
    return true;
  }
}

function kiemTraSo(value, idErr, message) {
  var reg = /^\d+$/;
  let isNumber = reg.test(value);
  if (isNumber) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}
