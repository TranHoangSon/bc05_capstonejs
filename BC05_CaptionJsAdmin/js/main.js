const BASE_URL = "https://635f4b17ca0fe3c21a991d02.mockapi.io";
let dataList = [];
// render
function fetchAllShop() {
  axios({
    url: `${BASE_URL}/Shop`,
    method: "GET",
  })
    .then(function (res) {
      res.data.forEach((item) => {
        dataList.push(item);
      });
      renderShopList(res.data);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
fetchAllShop();

// remove shoplist
function removeShop(idShop) {
  axios({
    url: `${BASE_URL}/Shop/${idShop}`,
    method: "DELETE",
  })
    .then(function (res) {
      fetchAllShop();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

// add ShopList
function addShopList() {
  var data = layThongTinTuForm();

  var newShop = {
    name: data.name,
    price: data.price,
    screen: data.screen,
    backCamera: data.backCamera,
    frontCamera: data.frontCamera,
    img: data.img,
    desc: data.desc,
    type: data.type,
  };

  var isValid = true;

  // valid name
  isValid =
    isValid &
    kiemTraRong(data.name, "nameErr", "Tên sản phẩm không được để rỗng");

  // valid giá
  isValid =
    kiemTraRong(data.price, "priceErr", "Giá sản phẩm không được để rỗng") &&
    kiemTraSo(data.price, "priceErr", "Giá sản phẩm phải là số");

  if (isValid) {
    axios({
      url: `${BASE_URL}/Shop`,
      method: "POST",
      data: newShop,
    })
      .then(function (res) {
        // gọi lại api get all shop
        fetchAllShop();
      })
      .catch(function (err) {
        console.log("err: ", err);
      });
  }
}

// edit ShopList

function editShopList(idShop) {
  var dom = document.getElementById("btnThem");
  dom.click();
  var viTri = timViTri(idShop, dataList);
  if (viTri == -1) return;

  var dataEdit = dataList[viTri];

  showThongTinLenForm(dataEdit);
}

// update
function updateShopList() {
  let dataUpdate = layThongTinTuForm();
  var idShop = dataUpdate.id;
  var viTri = timViTri(idShop, dataList);
  if (viTri == -1) return;

  dataList[viTri] = dataUpdate;

  axios({
    url: `${BASE_URL}/Shop/${idShop}`,
    method: "PUT",
    data: dataUpdate,
  })
    .then(function (res) {
      fetchAllShop();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
