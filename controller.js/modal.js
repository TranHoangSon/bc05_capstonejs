class CartItem {
  constructor(id, img, name, price, number) {
    this.id = id;
    this.img = img;
    this.name = name;
    this.price = price;
    this.number = number;
  }
  getNumber() {
    return this.number;
  }
  getPrice() {
    return this.price * this.number;
  }
}
