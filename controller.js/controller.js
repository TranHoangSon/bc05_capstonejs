function renderHTML(data) {
  let contentHTML = "";
  data.forEach((item) => {
    let contentDiv = `  <div class="item">
    <div class="wrapper">
    <div class="img">
      <img
        src="${item.img}"
        alt=""
      />
      </div>
      <div class="description" id=${item.id}>
        <div class="desc_item">
          <div class="item__title">
            <div class="product-name flex justify-between">
              <h5>${item.name}</h5>
              <button onclick="heart()"><i class="fas fa-heart" id="heart"></i>
              </button>
            </div>
            <div class="details hidden" type="${item.type}">
              <h5>Screen: ${item.screen}</h5>
              <h5>Back Camera: ${item.backCamera}</h5>
              <h5>Front Camera: ${item.frontCamera}</h5>
              <h5>Decription: ${item.desc}</h5>

              <div class="item__price flex justify-between">
                <h3>$ <span>${item.price}</span></h3>

                <button id="add" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded" onclick="add('${item.id}')">Add</button>
          
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>`;
    contentHTML += contentDiv;
  });
  document.getElementById("listItem").innerHTML = contentHTML;
}

function renderCards(data) {
  contentHTML = "";
  data.forEach((item) => {
    let contentTr = `<tr>
    <th><img src="${item.img}" alt="" style="width: 50%;height:100px" /></th>
    <th>${item.name}</th>
    <th>${item.price * item.number} $</th>
    <th><button onclick="decrease('${item.id}')">-</button> ${
      item.number
    } <button onclick="increase('${item.id}')">+</button></th>
    <th><button onclick="deleteItem('${
      item.id
    }')"><i class="fas fa-trash"></i></button></th>
    
    </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tbody-todos").innerHTML = contentHTML;
}
function search(id, data) {
  for (let i = 0; i < data.length; i++) {
    if (id == data[i].id) {
      return data[i];
    }
  }
  return -1;
}
function searchDelete(id, data) {
  for (let i = 0; i < data.length; i++) {
    if (id == data[i].id) {
      return i;
    }
  }
  return -1;
}
function totalItem(listItem) {
  let sum = 0;
  let total = 0;
  listItem.forEach((item) => {
    total = item.price * item.number;
    sum += total;
  });
  document.getElementById("total").innerText = sum;
  return sum;
}
function quantity(listItem) {
  let render = document.querySelector(".qty");
  let quantity = 0;
  listItem.forEach((item) => {
    quantity += item.number;
  });
  render.innerHTML = quantity;
}
