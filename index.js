const base_url = "https://635f4b17ca0fe3c21a991d02.mockapi.io";
let dataList = [];
let cartList = [];

function fetchAllProduct() {
  axios({
    url: `${base_url}/Shop`,
    method: "GET",
  })
    .then((res) => {
      renderHTML(res.data);

      renderCards(res.data);
      res.data.forEach((item) => {
        dataList.push(item);
      });
    })
    .catch((err) => {
      console.log(err);
    });
}
function chooseIphone() {
  let value = document.getElementById("countries").value;

  let select = [];
  let all = [];

  dataList.forEach((item) => {
    if (value == item.type) {
      select.push(item);
      renderHTML(select);
    }
  });

  console.log(all);
}

fetchAllProduct();
function add(id) {
  let dom = document.querySelectorAll("#addItem");

  let render = document.querySelector(".qty");
  document.querySelector(".content").style.display = "none";
  let quantity = 0;

  dom.forEach((item) => {
    item.style.display = "block";
    item.style.display = "flex";
  });
  if (checkExistingItem(id, cartList)) {
    let cartItem = search(id, cartList);
    cartItem.number += 1;
    cartList.forEach((item) => {
      quantity += item.number;
    });
    console.log(quantity);
    render.innerHTML = quantity;
  } else {
    let product = search(id, dataList);
    let newCartItem = new CartItem(
      product.id,
      product.img,
      product.name,
      product.price,
      1
    );
    cartList.push(newCartItem);
    // cartList.forEach((item) => {
    //   quantity += item.number;
    // });
    // console.log(quantity);
    // render.innerHTML = quantity;
    for (const item of cartList) {
      quantity += item.number;
    }
    render.innerHTML = quantity;
  }
  luuLocalStorage();
}

function buy() {
  document.getElementById("defaultModal").style.display = "block";

  renderCards(cartList);
  totalItem(cartList);
}

function checkExistingItem(id, data) {
  let isExisting = false;
  for (let i = 0; i < data.length; i++) {
    if (id == data[i].id) {
      isExisting = true;
    }
  }
  return isExisting;
}
function off() {
  let cart = document.getElementById("defaultModal");
  cart.style.display = "none";
}

function deleteItem(id) {
  let searchItem = searchDelete(id, cartList);

  let total = totalItem(cartList);

  if (searchItem !== -1) {
    cartList.splice(searchItem, 1);
    renderCards(cartList);

    totalItem(cartList);
    quantity(cartList);
  }
  luuLocalStorage();
}
function increase(id) {
  let addQuantity = search(id, cartList);
  addQuantity.number++;
  renderCards(cartList);
  totalItem(cartList);
  quantity(cartList);
}
function decrease(id) {
  let addQuantity = search(id, cartList);
  let searchItem = searchDelete(id, cartList);
  let index = addQuantity.number--;
  if (searchItem !== -1) {
    if (index == 1) {
      cartList.splice(searchItem, 1);
      renderCards(cartList);
    }
  }

  renderCards(cartList);
  totalItem(cartList);
  quantity(cartList);
  luuLocalStorage();
}
function clearItem() {
  cartList.splice(0, cartList.length);
  renderCards(cartList);
  totalItem(cartList);
  quantity(cartList);
  luuLocalStorage();
}

function luuLocalStorage() {
  let jsonDssv = JSON.stringify(cartList);
  localStorage.setItem("CART", jsonDssv);
}
var dataJson = localStorage.getItem("CART");

if (dataJson !== null) {
  var ItemArr = JSON.parse(dataJson);
  console.log(ItemArr);
  ItemArr.forEach((item) => {
    let arr = new CartItem(
      item.id,
      item.img,
      item.name,
      item.price,
      item.number
    );
    cartList.push(arr);
  });
  renderCards(cartList);
  quantity(cartList);
  document.querySelector(".content").style.display = "none";
}
